// @vitest-environment happy-dom

import insert from './insert.js';
import { test, describe, expect, it } from 'vitest';

let initialDisplay = test.extend({
  document: async ({}, use) => {
    document.body.innerHTML = '<div class="display">0</div>'
    await use(document)
    document.body.innerHTML = ''
  },
  state: async ({}, use) => {
    let state = { displayIsCleared: true }
    await use(state)
    state = { displayIsCleared: true }
  }
})

let partiallyFilledDisplay = test.extend({
  document: async ({}, use) => {
    document.body.innerHTML = '<div class="display">2 + </div>'
    await use(document)
    document.body.innerHTML = ''
  },
  state: async ({}, use) => {
    let state = { displayIsCleared: false }
    await use(state)
    state = { displayIsCleared: true }
  }
})

describe('#insert', () => {
  initialDisplay('replaces the expression on the display', ({ document, state }) => {
    expect(document.body.innerHTML).toBe('<div class="display">0</div>')
    expect(state.displayIsCleared).toBe(true)
    insert(1, state)
    expect(document.body.innerHTML).toBe('<div class="display">1</div>')
    expect(state.displayIsCleared).toBe(false)
  })
  partiallyFilledDisplay('appends a value to the expression on the display', ({ document, state }) => {
    expect(document.body.innerHTML).toBe('<div class="display">2 + </div>')
    expect(state.displayIsCleared).toBe(false)
    insert(2, state)
    expect(document.body.innerHTML).toBe('<div class="display">2 + 2</div>')
    expect(state.displayIsCleared).toBe(false)
  })
})
