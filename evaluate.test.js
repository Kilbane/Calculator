import evaluate from './evaluate.js';
import { test, describe, expect, it } from 'vitest';

describe('#evaluate', () => {
  it('correctly adds two positive numbers', () => {
    expect(evaluate('12+34')).toBe(46)
  })
  it('correctly adds a negative number with a positive number', () => {
    expect(evaluate('-12+34')).toBe(22)
  })
  it('correctly adds two negative numbers', () => {
    expect(evaluate('-12+-34')).toBe(-46)
  })
  it('correctly subtracts two positive numbers', () => {
    expect(evaluate('12-34')).toBe(-22)
  })
  it('correctly multiplies two positive numbers', () => {
    expect(evaluate('12x34')).toBe(408)
  })
})
