export default function insert(value, state) {
  let oldExpression = state.displayIsCleared ? "" : document.getElementsByClassName('display')[0].innerHTML;
  let newExpression = oldExpression.concat(value);
  document.getElementsByClassName("display")[0].innerHTML = newExpression;
  state.displayIsCleared = false;
}
