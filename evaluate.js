function process(stacks){
  let numberB = stacks.operandStack.pop();
  let numberA = stacks.operandStack.pop();
  let operator = stacks.operatorStack.pop();
  let result = 0.0;
  switch(operator){
    case "+":
      result = numberA + numberB;
      break;
    case "-":
      result = numberA - numberB;
      break;
    case "x":
      result = numberA * numberB;
      break;
    case "\xF7":
      result = numberA / numberB;
      break;
    case "%":
      result = 0.01 * numberA * numberB;
  }
  stacks.operandStack.push(result);
}

function tokenise(expression) {
  let characters = expression.split('');
  let tokens = [];
  let currentToken = '';
  for(let character of characters) {
    if (/\d|\./.test(character)) {
      currentToken += character;
    } else if (/\+|x|÷|%/.test(character)) {
      tokens.push(currentToken);
      tokens.push(character);
      currentToken = '';
    } else if (/\-/.test(character)) {
      if (/^-?\d+(\.?\d+)?$/.test(currentToken)) {
        tokens.push(currentToken);
        tokens.push(character);
        currentToken = '';
      } else {
        currentToken += character;
      }
    } 
  }
  if (currentToken !== '') {
    tokens.push(currentToken);
  }
  return tokens;
}

export default function evaluate(expression){
  let stacks = {
    operandStack: [],
    operatorStack: []
  };
  let operatorPrecedence = {"+":1, "-":1, "x":2, "\xF7":2, "%":3};
  let symbols = tokenise(expression)
  for(let symbol of symbols){
    switch(symbol){
      case "+":
      case "-":
      case "x":
      case "\xF7":
      case "%":
        if(stacks.operatorStack.length === 0){
          stacks.operatorStack.push(symbol)
        }
        else{
          if(operatorPrecedence[symbol] > operatorPrecedence[stacks.operatorStack[stacks.operatorStack.length - 1]]){
            stacks.operatorStack.push(symbol);
          }
          else{
            while(operatorPrecedence[symbol] <= operatorPrecedence[stacks.operatorStack[stacks.operatorStack.length - 1]]){
              process(stacks);
            }
            stacks.operatorStack.push(symbol);
          }
        }
        break;
      default:
        stacks.operandStack.push(parseFloat(symbol));
    }
  }
  while(stacks.operatorStack.length !== 0){
    process(stacks);
  }
  return stacks.operandStack.pop();
}
