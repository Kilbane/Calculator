import insert from './insert.js'
import cancel from './cancel.js'
import evaluate from './evaluate.js'

let state = {
  displayIsCleared: true
}

let insertButtons = document.querySelectorAll("button.insert");
insertButtons.forEach((button) => {
  button.addEventListener("click", () => {insert(button.innerHTML, state)}, false);
});

let cancelButtons = document.querySelectorAll("button.cancel");
cancelButtons.forEach((button) => {
  button.addEventListener("click", () => {cancel(state)}, false);
});

let evaluateButtons = document.querySelectorAll("button.evaluate");
evaluateButtons.forEach((button) => {
  button.addEventListener("click", () => {
    let expression = document.querySelectorAll(".display")[0].innerHTML;
    document.querySelectorAll(".display")[0].innerHTML = evaluate(expression);
  }, false);
});
